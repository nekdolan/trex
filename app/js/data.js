(function(){
	var setData = function() {
		cardColors = ['clubs','diamonds','hearts','spades'];
		cardNames = ['2','3','4','5','6','7','8','9','10','jack','queen','king','ace'];
		positionNames = ['top','bottom','left','right'];
		gameModes = ['trex','king_of_hearts','diamonds','dame','slaps'];
		gameModeNames = ['Trex','Piros szív király','Káró','Dáma','Ütés'];
	}
	if (typeof module !== 'undefined'){
		module.exports = setData;
	} else {
		setData();
	}
}());