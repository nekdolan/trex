/**
 * Created by Daniel on 2014.12.03..
 */

var $table = $('.table_container table tr');
var $card_container = $('.card_container');
var $last_card = $('.card_container > img');
var $spacer = $('.spacer');
var $my_cards_container = $('.my_cards');
var $game_area = $('.game_area');
var $popup_container = $('.popup_container');
var $popup = $('.popup');
var $choose_player = $('.choose_player');
var $choose_game_mode = $('.choose_game_mode');
var $waiting_for_players = $('.waiting_for_players');
var $info = $('body > .info');
var $bt_redraw = $('body > .bt_redraw');
var $game_mode_info = $('.game_mode_info');
var $status = $('body > .status');
var $show_results = $('.show_results');
var $results = $('.results');
var $table_cards = $('.positions > div > div');
var $player_container = $('.player_container');
var $restart = $('.restart');

var hash = location.hash.replace("#","");
var test = hash;
test = false;
var port = 8080;

var $positions = {
	top : $('.position_top'),
	bottom : $('.position_bottom'),
	left : $('.position_left'),
	right : $('.position_right')
};

var _init = function(fn){
	return function(obj){
		for(var attr in obj){
			if(obj.hasOwnProperty(attr)){
				this[attr] = obj[attr];
			}
		}
		if(fn){
			fn.call(this);
		}
	};
};

var MY_CARDS = (function(){
	var orderedCards = [];
	var _cards = [];
	var dragOptions = {
		scroll:false,
		containment: ".player_container > div",
//		containment: [0,0,0,-150],
		revert: "invalid",
		cursor: "move",
		disabled: true
	};

	var sortByName = function(cards, reverse){
		if(reverse){
			cards.sort(function(a,b){
				return cardNames.indexOf(b.name) - cardNames.indexOf(a.name);
			});
			return cards;
		}
		cards.sort(function(a,b){
			return cardNames.indexOf(a.name) - cardNames.indexOf(b.name);
		});
		return cards;
	};
	var _init_cards = {
		dropCard : function( event, ui ) {
			_cards.disableCards();
			var $card = (!ui ? event : ui.draggable);
			var orderedCardIndex = $my_cards_container.children().index($card);
			var cardIndex = _cards.indexOf(orderedCards[orderedCardIndex]);
			_cards.splice(cardIndex,1);
			orderedCards.splice(orderedCardIndex,1);
//			console.log(playedCard.getClassName() == $card.prop('class').split(' ')[1]);
			if(orderedCardIndex == (_cards.length)){
				orderedCardIndex -= 1;
			}
			$card.remove();
			$spacer.children().eq(orderedCardIndex).remove();
			$table.children().eq(orderedCardIndex).remove();
			_cards.reLocate();
			GAME.setNextCard(cardIndex);
		},
		reLocate : function(){
			orderedCards.forEach(function(card, index){
				var img = $card_container.find('img').eq(index);
				var cardsStyle = card.renderCard(img.offset(), true);
				$my_cards_container.children().eq(index).attr('style',cardsStyle);
			});
			return _cards;
		},
		reDraw : function(){
			var cardsHtml = '';
			orderedCards.forEach(function(card, index){
				var img = $card_container.find('img').eq(index);
				cardsHtml += card.renderCard(img.offset());
			});
			$my_cards_container.html(cardsHtml);
			$my_cards_container.children().draggable(dragOptions);
			setTimeout(function(){
				_cards.reLocate();
			},0);
			return _cards;
		},
		orderCards : function(){
			orderedCards = [];
			var colors = {'diamonds':[],'clubs':[],'hearts':[],'spades':[]};
			var lastColor = null;
			var firstColor = null;
			_cards.forEach(function(card){
				colors[card.color].push(card);
			});
			for(var attr in colors){
				sortByName(colors[attr]);
			}
			if(!colors.clubs.length){
				lastColor = 'hearts';
			}
			if(!colors.hearts.length){
				firstColor = 'clubs';
			}
			if(firstColor){
				orderedCards = orderedCards.concat(colors[firstColor]);
			}
			for(var attr in colors){
				if(attr != firstColor && attr != lastColor){
					orderedCards = orderedCards.concat(colors[attr]);
				}
			}
			if(lastColor){
				orderedCards = orderedCards.concat(colors[lastColor]);
			}
			return _cards;
		},
		renderAllCards : function(){
			_cards.orderCards();
			var spacerHtml = '<img src="images/empty.png"/>';
			var tableHtml = '';
			for(var i=1; i<orderedCards.length; i++){
				tableHtml += '<td><img src="images/empty.png"/></td>';
				spacerHtml += '<img src="images/empty.png"/>';
			}
			$spacer.html(spacerHtml);
			$table.html(tableHtml);
			setTimeout(function(){
				_cards.reDraw();
			},0);
			return _cards;
		},
		renderCardsOnTrexTable : function(data){
			var cardsOnTable = data.cardsOnTable;
			var roundStarterIndex = data.roundStarterIndex;
			var colors = {'diamonds':[],'clubs':[],'hearts':[],'spades':[]};
			cardsOnTable.forEach(function(card){
				colors[card.color].push(card);
			});
			for(var attr in colors){
				var colorCards = colors[attr];
				sortByName(colorCards, true);
				if(colorCards.length == 0){
					continue;
				}
				var $area = $('.'+attr+'_area');
				var topCard = new CARD(colorCards[0]);
				$area.children('.top_card').prop('class', 'card top_card '+topCard.getClassName());
				if(colorCards.length > 1){
					var bottomCard = new CARD(colorCards[colorCards.length-1]);
					if(colorCards.length == 2){
						$area.children('.extra_cards').prop('class','extra_cards off');
					} else {
						$area.children('.extra_cards').prop('class','extra_cards on');
					}
					$area.children('.bottom_card').prop('class', 'card bottom_card '+bottomCard.getClassName());
				}
			}
			return _cards;
		},
		renderCardsOnTable : function(data){
			if(GAME.currentGameMode == 'trex'){
				return _cards.renderCardsOnTrexTable(data);
			}
			var cardsOnTable = data.cardsOnTable;
			var roundStarterIndex = data.roundStarterIndex;
			for(var i=0; i<4; i++){
				var card = cardsOnTable[i] ? new CARD(cardsOnTable[i]) : null;
				var index = (roundStarterIndex+i)%4;
				GAME.players[index].renderTableCard(card, i);
			}
			return _cards;
		},
		clearTable : function(){
			$game_area.find('.top_card').prop('class','card top_card');
			$game_area.find('.extra_cards').prop('class','extra_cards');
			$game_area.find('.bottom_card').prop('class','card bottom_card');
			$table_cards.prop('class','');
		},
		disableCards : function(){
			$my_cards_container.children().draggable( "disable" );
		},
		enableCards : function(acceptableCards){
			var myAcceptableCards = acceptableCards || MY_CARDS.acceptableCards;
			_cards.acceptableCards = myAcceptableCards;
			_cards.disableCards();
			var orderedAcceptableCards = [];
			myAcceptableCards.forEach(function(acceptableCard){
				var cardIndex = orderedCards.indexOf(_cards[acceptableCard]);
				orderedAcceptableCards.push(cardIndex);
				$my_cards_container.children().eq(cardIndex).draggable('enable');
			});
			return orderedAcceptableCards;
		}
	};
	var dropOptions = {
		drop: _init_cards.dropCard
	};
	$game_area.droppable(dropOptions);
	$my_cards_container.on( 'dblclick','.card', function(event){
		event.stopPropagation();
        if($(this).hasClass('ui-draggable-disabled')){
        	return;
		}
		_cards.dropCard($(this));
	});
//	$('body').droppable(dropOptions);

	_init().call(_cards, _init_cards);

	$( window ).resize(function() {
		_cards.reLocate();
	});
	return _cards;
}());

var CARD = (function(){
	var _card = _init();
	_card.prototype = {
		getClassName : function(){
			return 'card_'+this.name+'_of_'+this.color;
		},
		renderCard : function(position, getPos){
			var pos = position || this.getPosition();
			var posHtml = 'top: '+position.top+'px; left: '+position.left+'px';
			if(getPos){
				return posHtml;
			}
			var html = '<div style="' + posHtml + '" ' +
				'class="card '+this.getClassName()+'">' +
				'<img src="images/empty.png"></div>';
			return html;
		},		
		getIndex : function(){
			var index = -1;
			var self = this;
			MY_CARDS.each(function(cardIndex, card){
				if(card == self){
					index = cardIndex;
					return false;
				}
			});
			return index;
		},
		getPosition : function(){
			var index = this.getIndex();
			var img = $card_container.find('img').eq(index);
			return img.position();
		}
	}
	return _card;
}());

var Player = (function(){
	var _player = _init();
	_player.prototype = {
		renderTableCard : function(card, index){
			var name = card ? card.getClassName() : '';
			$positions[this.position]
				.css('z-index', index+1)
				.children()
				.prop('class',name);
		},
		nextPlayer : function(){
			var playerIndex = GAME.players.indexOf(this);
			playerIndex = (playerIndex + 1) % 4;
			return GAME.players[playerIndex];
		},
		beforePlayer : function(){
			var playerIndex = GAME.players.indexOf(this) - 1;
			if(playerIndex < 0){
				playerIndex = 3;
			}
			return GAME.players[playerIndex];
		},
		setAsActive : function(){
			GAME.activePlayer = this;
			$('.avatar + .image').removeClass('on');
			$('.'+this.position+'_player .image').addClass('on');
		},
		decreaseCardNumber : function(){
			this.numOfCards -= 1;
			$('.'+this.position+'_player .card_number').html(this.numOfCards);
		}
	}

	return _player;
}());

var GAME = (function(){
	var socket = null;
	var _game = {
		players : [],
		playerId : null,
		player : null,
		currentGameMode : null,
		score : [0,0,0,0],
		active : false,
		activePlayer : null,
		setNextCard : function(cardIndex){
			socket.emit('set_next_card',cardIndex);
			_game.active = false;
		},
		setScore : function(score){
			var $columns = $results.find('tr').eq(1).children();
			score.forEach(function(points, index){
				$columns.eq(index).html(points);
			});
		},
		init : function(){
			socket = io(':'+port);
			socket.on('player_disconnected',function(name){
//				alert(name+' kilépett vagy kiesett');
				socket.disconnect();
			});
			socket.on('game_end', function(score){
//				$restart.show();
				_game.setScore(score);
				$info.click();
				$show_results.addClass('end_game');
				socket.disconnect();
			});
			socket.on('message',function(text){
				$status.html(text);
			});
			socket.on('table_change', function(data){
				$status.html('...');
				MY_CARDS.renderCardsOnTable(data);
				_game.activePlayer.decreaseCardNumber();
			});
			socket.on('get_next_card', function(acceptableCards){
				$('.avatar + .image').removeClass('on');
				var orderedAcceptableCards = MY_CARDS.enableCards(acceptableCards);
				_game.active = true;
				_game.activePlayer = _game.player;

				if(test){
					setTimeout(function(){
						var cardIndex = orderedAcceptableCards[Math.floor(Math.random() * orderedAcceptableCards.length)];
						MY_CARDS.dropCard($my_cards_container.children().eq(cardIndex));
					},25);
				}
			});
			socket.on('waiting_for_player', function(data){
				var playerId = data.playerId;
				var activePlayer = null;
				_game.players.some(function(player){
					if(playerId == player.id){
						player.setAsActive();
						activePlayer = player;
						return false;
					}
				});
				if(data.message){
					$status.html(data.message);
				}
				if(data.score) {
					_game.setScore(data.score);
				}
			});
			socket.on('cards',function(cards){
				MY_CARDS.clearTable();
				MY_CARDS.splice(0,MY_CARDS.length);
				cards.forEach(function(cardData, index){
					cardData.index = index;
					MY_CARDS.push(new CARD(cardData));
				});
				_game.players.forEach(function(player){
					player.numOfCards = 13;
				});
				$('.card_number').html(13);
				MY_CARDS.renderAllCards();
			});
			socket.on('mode', function(modeName){
				_game.currentGameMode = modeName;
				$game_mode_info.html(gameModeNames[gameModes.indexOf(modeName)]);
				$status.html('...');
			});
			socket.on('get_game_mode', function(data){
				$popup_container.removeClass('off');
				$popup.children().removeClass('active');
				$choose_game_mode.children()
					.removeClass('btn_on')
					.each(function(){
						var gameModeName = $(this).data('game_mode');
						if(data.availableGameModes.indexOf(gameModeName) != -1){
							$(this).addClass('btn_on');
						}
					});
				$choose_game_mode
					.addClass('active')
					.one('click','.btn_on.btn', function(){
						var gameModeName = $(this).data('game_mode');
						socket.emit('set_game_mode',gameModeName);
						$popup_container.addClass('off');
					});
				_game.setScore(data.score);
				if(test){
					var $buttons = $choose_game_mode.children('.btn_on');
					var index = Math.floor(Math.random() * $buttons.length);
					$buttons.eq(index).click();
				}
			});
			socket.on('set_players', function(data){
				data.forEach(function(playerData){
					_game.players.push(new Player(playerData));
				});
				_game.players.some(function(player){
					if(_game.playerId == player.id){
						_game.player = player;
						player.position = 'bottom';
						player.nextPlayer().position = 'right';
						player.beforePlayer().position = 'left';
						player.nextPlayer().nextPlayer().position = 'top';
						return false;
					}
				});
				var $columns = $results.find('tr').first().children();
				_game.players.forEach(function(player, index){
					$columns.eq(index).html(player.name);
					if(_game.playerId != player.id){
						$('.'+player.position+'_player .avatar').css('background-image','url(./images/users/'+player.id+'.jpg)');
					}
				});
			});
			socket.on('start_game', function(){
				$popup_container.addClass('off');
			});
			socket.on('waiting_for_players',function(wait){
				if(wait){
					$('.choose_player').off();
					$popup.children().removeClass('active');
					$waiting_for_players.addClass('active');
				} else {
					alert('Ezt a nevet már kiválasztották!');
				}
			});
			socket.on('connect', function () {
				$('.choose_player').on('click','.btn', function(){
					var playerData = {
						name : $(this).html(),
						id : $(this).data('id')
					};
					_game.playerId = playerData.id;
					socket.emit('login', playerData);
				});

				if(test){
					$('.choose_player .btn').eq(hash*1).click();
//					hash = false;
				}
			});
			$info.click(function(){
				$popup_container.removeClass('off');
				$popup.children().removeClass('active');
				$show_results.addClass('active');
			});
			$bt_redraw.click(function(){
				if(MY_CARDS.length){
					MY_CARDS.orderCards();
					MY_CARDS.reDraw();
					if(_game.active){
						MY_CARDS.enableCards();
					}
				}
			});
			$show_results.find('.btn').click(function(){
				$popup_container.addClass('off');
			});
			$restart.click(function(){
				window.location.href = window.location.pathname;
//				$show_results.removeClass('end_game');
			});
		}
	};
	return _game;
}());

GAME.init();