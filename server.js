require('./app/js/data.js')();
var app = require('net').createServer();
var io = require('socket.io')(app);
var spawn = require('child_process').spawn;
var static = require('node-static');
var port = process.argv[2] || 8080;

var file = new static.Server('./app');

require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        //
        // Serve files!
        //
        file.serve(request, response);
    }).resume();
}).listen(80);

var _init = function(fn){
	return function(obj){
		for(var attr in obj){
			if(obj.hasOwnProperty(attr)){
				this[attr] = obj[attr];
			}
		}
		if(fn){
			fn.call(this);
		}
	};
};
Array.prototype.mix = function(){
	var arr = this;
	var res = [];
	while(arr.length > 0){
		var index = Math.floor(Math.random()*arr.length);
		res.push(arr.splice(index,1)[0]);
	}
	for(var i=0; i<res.length; i+=1){
		arr.push(res[i]);
	}
	return arr;
};

var GAME =  (function(){
	var game = _init();
	game.prototype = {
		init : function(playersData){
			var game = this;
			game.players = null;
			game.currentPlayer = null;
			game.currentGameMode = null;
			game.players = [];
			playersData.mix();
			playersData.forEach(function(playerData, index){
				var player = new PLAYER(playerData);
				player.game = game;
				game.players.push(player);
				delete playerData.socket;
			});
//			io.emit('set_players', playersData);
			game.emit('set_players', playersData);
			game.initNextPlayer();
		},
		emit : function(name, data, exclude){
			var game = this;
			game.players.forEach(function(player){
				if(player == exclude){
					return;
				}
				player.socket.emit(name, data);
			});
		},
		getNextPlayer : function(pl){
			var game = this;
			var player = pl || game.currentPlayer;
			var index = game.players.indexOf(player);
			return game.players[(index + 1) % game.players.length];
		},
		initNextPlayer : function(){
			var game = this;
			if(game.currentPlayer){
				var player = game.getNextPlayer();
				if(player == game.players[0]){
					game.end();
					return;
				}
				game.currentPlayer = player;
			} else {
				game.currentPlayer = game.players[0];
			}
			game.currentPlayer.getNextGameMode();
		},
		dealCards : function(){
			var game = this;
			var allCards = [];
			var test = [];
			cardColors.forEach(function(color){
				cardNames.forEach(function(name){
					allCards.push(
						new CARD({
							'color' : color,
							'name' : name
						})
					);
				});
			});
			allCards.mix();
//			var testCards = [
//				[{"color":"spades","name":"9"},{"color":"clubs","name":"king"},{"color":"diamonds","name":"jack"},{"color":"diamonds","name":"king"},{"color":"spades","name":"jack"},{"color":"hearts","name":"king"},{"color":"hearts","name":"ace"},{"color":"diamonds","name":"3"},{"color":"diamonds","name":"10"},{"color":"spades","name":"3"},{"color":"clubs","name":"jack"},{"color":"spades","name":"6"},{"color":"clubs","name":"7"}],
//				[{"color":"hearts","name":"5"},{"color":"spades","name":"5"},{"color":"clubs","name":"10"},{"color":"clubs","name":"8"},{"color":"clubs","name":"4"},{"color":"clubs","name":"3"},{"color":"spades","name":"8"},{"color":"diamonds","name":"6"},{"color":"hearts","name":"queen"},{"color":"spades","name":"queen"},{"color":"hearts","name":"10"},{"color":"diamonds","name":"4"},{"color":"hearts","name":"3"}],
//				[{"color":"hearts","name":"8"},{"color":"diamonds","name":"queen"},{"color":"clubs","name":"queen"},{"color":"hearts","name":"9"},{"color":"diamonds","name":"7"},{"color":"hearts","name":"6"},{"color":"diamonds","name":"9"},{"color":"hearts","name":"4"},{"color":"clubs","name":"ace"},{"color":"spades","name":"7"},{"color":"spades","name":"10"},{"color":"diamonds","name":"5"},{"color":"spades","name":"ace"}],
//				[{"color":"spades","name":"2"},{"color":"clubs","name":"5"},{"color":"diamonds","name":"8"},{"color":"spades","name":"4"},{"color":"hearts","name":"7"},{"color":"clubs","name":"2"},{"color":"spades","name":"king"},{"color":"clubs","name":"6"},{"color":"diamonds","name":"ace"},{"color":"hearts","name":"2"},{"color":"hearts","name":"jack"},{"color":"clubs","name":"9"},{"color":"diamonds","name":"2"}]
//			];

			game.players.forEach(function(player, index){
				player.cards = allCards.slice(index*13, (index+1)*13);
//				var cards = testCards[index];
//				var newCards = [];
//				cards.forEach(function(card){
//					newCards.push(new CARD(card));
//				});
//				player.cards = newCards;
				player.discardedCards = [];
				player.socket.emit('cards', player.cards);
			});
		},
		startGameMode : function(data){
			var game = this;
			if(data === null){
				game.initNextPlayer();
			}
//			io.emit('mode', data.gameModeName);
			game.emit('mode', data.gameModeName);
			data.game = game;
			game.currentGameMode = new GAME_MODE(data);
		},
		end : function(){
			var game = this;
//			if(DISCONNECTS != 0){
//				return;
//			}
			var score = game.getScore();
//			io.emit('game_end',score);
			game.emit('game_end',score);
		},
		getScore : function(){
			var game = this;
			var score = [];
			game.players.forEach(function(pl){
				score.push(pl.points);
			});
			return score;
		}
	};
	return game;
}());

var GAME_MODE = (function(){
	var ruleColor = function(options){
		var card = options.card;
		if(!this.cardsOnTable.length){
			acceptAll(options);
			return false;
		}
		if(this.cardsOnTable[0].color == card.color){
			return true;
		}
		return false;
	};
	var ruleColorException = function(options){
		if(!options.acceptableCards.length){
			acceptAll(options);
		}
		return false;
	};
	var ruleJack = function(options){
		var card = options.card;
		if(card.name == 'jack'){
			return true;
		}
		return false;
	};
	var ruleTrex = function(options){
		var largest = { clubs:null, diamonds:null, hearts: null, spades : null };
		var smallest = { clubs:null, diamonds:null, hearts: null, spades : null };
		this.cardsOnTable.forEach(function(card, i){
			var nameIndex = cardNames.indexOf(card.name);
			if(largest[card.color] === null ||  largest[card.color] < nameIndex){
				largest[card.color] = nameIndex;
			}
			if(smallest[card.color] === null ||  smallest[card.color] > nameIndex){
				smallest[card.color] = nameIndex;
			}
		});
		options.cards.forEach(function(card, i){
			var nameIndex = cardNames.indexOf(card.name);
			if((largest[card.color] !== null && nameIndex == largest[card.color]+1) ||
				(smallest[card.color] !== null && nameIndex == smallest[card.color]-1)){
				options.acceptableCards.push(i);
			}
		});
		options.skip = true;
		return false;
	};
	var acceptAll = function(options){
		options.cards.forEach(function(card, i){
			options.acceptableCards.push(i);
		});
		options.skip = true;
	};
	var hasTheseCards = function(cards, pattern, number){
		var numberOfCards = 0;
		var endSuccess = false;
		cards.some(function(card){
			if(card.matchPattern(pattern)){
				numberOfCards += 1;
				if(numberOfCards == number){
					endSuccess = true;
					return true;
				}
			}
		});
		return endSuccess;
	};
	var countPointsForThese = function(cards, pattern, point){
		var points = 0;
		cards.forEach(function(card){
			if(card.matchPattern(pattern)){
				points += point;
			}
		});
		return points;
	};
	var gameModePatterns = {
		king_of_hearts : {color: 'hearts', name : 'king'},
		diamonds : {color: 'diamonds'},
		dame : {name: 'queen'},
		slaps : {}
	};
	var gameModes = {
		trex : { rules : {jack : ruleJack, trex : ruleTrex},
			endRules : function(){
				return this.cardsOnTable.length == 52;
			},
			countPoints : function(){
				var gameMode = this;
				for(var i=0; i<4; i+=1){
					gameMode.finishedPlayers[i].points += (4-i)*50;
				}
			},
			failRules : function(cards){

			}
		},
		king_of_hearts : {rules : {color :  ruleColor, colorException : ruleColorException},
			endRules : function(discardedCards){
				return hasTheseCards(discardedCards, gameModePatterns.king_of_hearts, 1);
			},
			countPoints : function(discardedCards){
				return countPointsForThese(discardedCards, gameModePatterns.king_of_hearts, -75);
			}
		},
		diamonds : {rules : {color :  ruleColor, colorException : ruleColorException},
			endRules : function(discardedCards){
				return hasTheseCards(discardedCards, gameModePatterns.diamonds, 13);
			},
			countPoints : function(discardedCards){
				return countPointsForThese(discardedCards, gameModePatterns.diamonds, -10);
			}
		},
		dame: {rules : {color :  ruleColor, colorException : ruleColorException},
			endRules : function(discardedCards){
				return hasTheseCards(discardedCards, gameModePatterns.dame, 4);
			},
			countPoints : function(discardedCards){
				return countPointsForThese(discardedCards, gameModePatterns.dame, -25);
			}
		},
		slaps: {rules : {color :  ruleColor, colorException : ruleColorException},
			endRules : function(discardedCards){
				return discardedCards.length == 52;
			},
			countPoints : function(discardedCards){
				return discardedCards.length * -15 / 4;
			}
		}
	};
	var _game_mode = _init(function(){
		if(this.gameModeName == 'trex'){
			this.finishedPlayers = [];
		}
		this.mode = gameModes[this.gameModeName];
		this.players = this.game.players;
		this.round = 0;
		this.roundStart(this.caller);
	});
	_game_mode.prototype = {
		getAcceptableCards : function(){
			var gameMode = this;
			var acceptableCards = [];
			for(var ruleName in gameMode.mode.rules){
				var rule = gameMode.mode.rules[ruleName];
				gameMode.activePlayer.cards.some(function(card, i){
					var options = {
						card : card,
						cards : gameMode.activePlayer.cards,
						acceptableCards : acceptableCards
					};
					if(rule.call(gameMode, options)){
						acceptableCards.push(i);
					}
					if(options.skip){
						return true;
					}
				});
			}
			return acceptableCards;
		},
		getNextPlayer : function(){
			return this.game.getNextPlayer(this.activePlayer);
		},
		roundStart : function(starter){
			this.round += 1;
			this.cardsOnTable = [];
			this.roundStarter = starter;
			this.activePlayer = starter;
			this.getNextCard();
		},
		getNextCard : function(){
			var gameMode = this;
			if(this.gameModeName == 'trex' && this.finishedPlayers.indexOf(this.activePlayer) != -1){
				gameMode.activePlayer = gameMode.getNextPlayer();
				gameMode.getNextCard();
				return;
			}
			this.acceptableCards = this.getAcceptableCards();
			if(this.gameModeName == 'trex' && !this.acceptableCards.length){
				gameMode.activePlayer.knock();
				gameMode.activePlayer = gameMode.getNextPlayer();
				gameMode.getNextCard();
				return;
			}
			this.activePlayer.getNextCardIndex(this.acceptableCards);
		},
		playNextCard : function(index){
			var gameMode = this;
			var card = gameMode.activePlayer.cards[index];
			gameMode.activePlayer.cards.splice(index,1);
			gameMode.cardsOnTable.push(card);
//			io.emit('table_change', {
			gameMode.game.emit('table_change', {
				cardsOnTable : gameMode.cardsOnTable,
				roundStarterIndex : gameMode.players.indexOf(gameMode.roundStarter)
			});
			if(gameMode.gameModeName != 'trex'){
				if(gameMode.cardsOnTable.length == 4){
					gameMode.roundEnd();
					return;
				}
			} else {
				if(!gameMode.activePlayer.cards.length){
					this.finishedPlayers.push(gameMode.activePlayer);
				}
				if(gameMode.mode.endRules.call(gameMode)){
					gameMode.mode.countPoints.call(gameMode);
					gameMode.game.currentPlayer.getNextGameMode();
					return;
				}
			}
			gameMode.activePlayer = gameMode.getNextPlayer();
			gameMode.getNextCard();
		},
		roundEnd : function(){
			var gameMode = this;
			var color = this.cardsOnTable[0].color;
			var currentLargestCard = this.cardsOnTable[0];
			this.cardsOnTable.forEach(function(card){
				var newIndex = cardNames.indexOf(card.name);
				if((currentLargestCard.color == card.color)
					&& (newIndex > cardNames.indexOf(currentLargestCard.name))){
					currentLargestCard = card;
				}
			});
			var cardIndex = this.cardsOnTable.indexOf(currentLargestCard);
			var endPlayer = this.players[(this.players.indexOf(this.roundStarter) + cardIndex)%4];
			endPlayer.discardedCards = endPlayer.discardedCards.concat(this.cardsOnTable);
			var allDiscardedCards = [];
			this.players.forEach(function(player){
				allDiscardedCards = allDiscardedCards.concat(player.discardedCards);
			});
			if(this.mode.endRules.call(this, allDiscardedCards)){
				this.players.forEach(function(player){
					player.points += gameMode.mode.countPoints(player.discardedCards);
				});
				gameMode.game.currentPlayer.getNextGameMode();
				return;
			}
			this.roundStart(endPlayer);
		}
	};
	return _game_mode;
}());

var CARD = (function(){
	var _card = _init();
	_card.prototype = {
		matchPattern : function(pattern){
			var card = this;
			if((!pattern.color || pattern.color == card.color) &&
				(!pattern.name || pattern.name == card.name)){
				return true;
			}
			return false;
		}
	};
	return _card;
}());

var PLAYER = (function(){
	var _player = _init(function(){
		var player = this;
		player.points = 0;
		player.availableGameModes = gameModes.slice(0);
		player.socket.on('disconnect', function(){
//			DISCONNECTS += 1;

			player.game.emit('player_disconnected',{name: player.name}, player);
			player.game.end();
		});
		player.socket.on('set_game_mode', function(gameModeName){
			var canceler = null;
			switch(gameModeName){
				case 'trex' :
					player.game.players.forEach(function(onePlayer){
						if(player == onePlayer){
							return;
						}
						var twos = [];
						var threes = [];
						onePlayer.cards.forEach(function(card){
							if(card.name == '2'){
								twos.push(card);
							}
							if(card.name == '3'){
								threes.push(card);
							}
						});
						if(twos.length == 4){
							canceler = onePlayer;
						}
						if(twos.length == 3){
							threes.forEach(function(cardThree){
								var match = false;
								twos.forEach(function(cardTwo){
									if(cardThree.color == cardTwo.color){
										match = true;
									}
								});
								if(!match){
									canceler = onePlayer;
								}
							});
						}
					});
					break;
				case 'king_of_hearts' :
					player.game.players.forEach(function(onePlayer){
						if(player == onePlayer){
							return;
						}
						var badCards = 0;
						var goodCards = 0;
						onePlayer.cards.forEach(function(card){
							if(card.color == 'hearts'){
								if(card.name == 'king' || card.name == 'ace'){
									badCards += 1;
								} else {
									goodCards += 1;
								}
							}
						});
						if(goodCards == 0 && badCards > 0){
							canceler = onePlayer;
						}
					});
					break;
			}
			if(canceler){
				player.getNextGameMode(canceler);
				return;
			}
			var index = player.availableGameModes.indexOf(gameModeName);
			var nextGameModeName = player.availableGameModes.splice(index,1)[0];
			player.game.startGameMode({
				gameModeName: nextGameModeName,
				caller: player
			});
		});
		player.socket.on('set_next_card', function(cardIndex){
			player.game.currentGameMode.playNextCard(cardIndex);
		});
		player.socket.emit('start_game');
	});
	_player.prototype = {
		getNextGameMode : function(canceler){
			var player = this;
			if(player.availableGameModes.length == 0){
				player.game.initNextPlayer();
				return;
			}
			var score = player.game.getScore();
			player.game.dealCards();
			player.socket.emit('get_game_mode',{
				availableGameModes: player.availableGameModes,
				score : score
			});
			var message = (canceler ? canceler.name+' bedopta a lapjait!' : player.name+' választ éppen...');
			player.socket.broadcast.emit('waiting_for_player',{
				playerId : player.id,
				score : score,
				message : message
			});
		},
		getNextCardIndex : function(acceptableCards){
			var player = this;
			player.socket.emit('get_next_card',acceptableCards);
			player.socket.broadcast.emit('waiting_for_player',{
				playerId : player.id
			});
		},
		knock : function(){
			io.emit('message', this.name+' kopogott.');
		}
	};
	return _player;
}());

//var DISCONNECTS = 0;
var PLAYERS_DATA = [];

io.on('connection', function (socket) {
	socket.on('login', function(playerData){
		if(PLAYERS_DATA.length == 4){
			PLAYERS_DATA = [];
		}
		if(PLAYERS_DATA.some(function(data, i){
			if(data.id == playerData.id){
				return true;
			}
		})){
			socket.emit('waiting_for_players', false);
			return;
		}
		playerData.socket = socket;
		PLAYERS_DATA.push(playerData);
		if(PLAYERS_DATA.length == 4){
//			DISCONNECTS = 0;
			new GAME().init(PLAYERS_DATA);
//			GAME.init(PLAYERS_DATA);
		} else {
			socket.emit('waiting_for_players', true);
		}
	});
});

io.listen(port);
